const mongoose = require('mongoose');
const config = require('./config');

const Product = require('./models/Product');

const run = async () => {
    await mongoose.connect(config.productDb, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    await Product.create(
        {
            title: 'iMac',
            price: 206000,
            description: 'The powerful iMac features an ultra-thin all-in-one design, beautiful widescreen display, the latest processors and graphics, and advanced storage ...',
            image: 'iMac.jpeg'
        },
        {
            title: 'MI Notebook',
            price: 80000,
            description: 'If you\'re prepared to spend some time anglicising your machine, the Mi Notebook Pro offers excellent value for money.',
            image: 'mi_notebook.jpg'
        },
        {
            title: 'MI Notebook Pro',
            price: 64000,
            description: 'USD 979.99 , Wholesale Price, Xiaomi Mi Notebook Pro 15.6 Fingerprints Intel Core i5-8250U 3.4GHz 8GB RAM 256GB SSD ROM Windows 10 4 NVMe SSD USB-C ...',
            image: 'mi_notebookPro.jpg'
        },
        {
            title: 'Iphone Xs',
            price: 126000,
            description: 'iPhone XS Max features a 6.5-inch Super Retina display with custom-engineered OLED panels for an HDR display that provides the industry\'s best colour ...',
            image: 'iphone_xs.jpg'
        },
        {
            title: 'Samsung S10',
            price: 120000,
            description: 'Best price for Samsung Galaxy S10 is Rs. 61,900 as on 4th July 2019.',
            image: 'samsung_s10.jpg'
        },
        {
            title: 'Redmi 7',
            price: 13000,
            description: 'Xiaomi has updated its low-cost workhorse Redmi series with the new Redmi 7',
            image: 'Redmi-7.jpg'
        },
        {
            title: 'Redmi Note 7',
            price: 15000,
            description: 'The Redmi Note 7 features a glass front and back with Corning Gorilla Glass 5 protection on both sides.',
            image: 'redmi_note_7.jpg'
        },
        {
            title: 'Apple Watch',
            price: 50000,
            description: 'Apple - Apple Watch Series 4 (GPS) 40mm Space Gray Aluminum Case with Black Sport Band - Space Gray ',
            image: 'apple_watch.jpg'
        },
        {
            title: 'Mi Band',
            price: 4000,
            description: 'The Mi Band - HRX Edition is a watch and a fitness tracker built into one smart band. ',
            image: 'mi_Band.jpg'
        },
        {
            title: 'Men\'s Watch',
            price: 3800,
            description: 'No description',
            image: 'watch.jpg'
        }
    );

    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});
