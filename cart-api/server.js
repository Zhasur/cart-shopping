const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const config = require('./config');

const product = require('./app/product');

const app = express();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());

const port = 8000;

mongoose.connect(config.productDb, config.mongoOptions).then(() => {
    app.use('/product', product);


    app.listen(port, () => {
        console.log('Server is running on port' + port);
    });
});