const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    productDb: 'mongodb://localhost/product',
    mongoOptions: {
        useNewUrlParser: true,
        useCreateIndex: true,
    }
};