const express = require('express');
const router = express.Router();
const multer = require('multer');
const config = require('../config');
const nanoid = require('nanoid');
const path = require('path');

const Product = require('../models/Product');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', (req, res) => {
    Product.find()
        .then(result => res.send(result))
        .catch(error => res.send(error))
});

router.post('/', upload.single('image'), (req, res) => {
    const productData = req.body;

    if (req.file) {
        productData.image = req.file.filename;
    }

    const product = new Product(productData);

    product.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

router.delete('/:id', (req, res) => {
   Product.findOneAndDelete({_id: req.params.id})
       .then(result => res.send(result))
       .catch(error => res.send(error));
});

module.exports = router;