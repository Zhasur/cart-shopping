import axios from "../../products-axios"

export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";


export const fetchProductsSuccess = products => ({type: FETCH_PRODUCTS_SUCCESS, products});

export const fetchProducts = () => {
    return dispatch => {
        return axios.get("/product").then(
            response => {
                dispatch(fetchProductsSuccess(response.data))
            }
        );
    };
};

export const deleteProduct = (id) => {
    return dispatch => {
        return axios.delete("/product/" + id).then(
            response => {
                dispatch(fetchProducts())
            }
        );
    };
};