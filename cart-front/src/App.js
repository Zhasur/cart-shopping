import React from 'react';
import {Route, Switch} from "react-router-dom";
import Products from "./container/Products/Products";

function App() {
  return (
      <Switch>
          <Route path="/" exact component={Products}/>
      </Switch>
  );
}

export default App;
