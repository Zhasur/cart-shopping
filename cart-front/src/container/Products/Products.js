import React, {Component} from "react"
import {connect} from "react-redux";
import {fetchProducts, deleteProduct} from "../../store/actions/ProductsAction";

import './Products.css'

class Products extends Component {
    componentDidMount() {
        this.props.onFetchProducts();
    }

    onDeleteHandler = (id) => {
        this.props.deleteProduct(id);
        this.props.history.push('/');
    };

    render() {

        console.log(this.props.products);
        let productsArray = this.props.products ?  this.props.products.map(product => {
            return (
                <div key={product._id} className="product">
                    <img className="product-img" src={"http://localhost:8000/uploads/" + product.image} alt="product"/>
                    <div className="product-txt">
                        <span className="product-name">{product.title}</span>
                        <span className="product-price">{product.price} som</span>
                        <span className="product-description">{product.description}</span>
                    </div>
                    <button onClick={() => this.onDeleteHandler(product._id)} className="delete-btn">x</button>
                </div>
            )
        }): null;

        return (
                <div className="products-block">
                    <h2 className="products-title">Products cart</h2>
                    <div className="products">
                        {productsArray}
                    </div>
                </div>
        )
    }
}

const mapStateToProps = state => ({
    products: state.products
});

const mapDispatchToProps = dispatch => ({
    onFetchProducts: () => dispatch(fetchProducts()),
    deleteProduct: (id) => dispatch(deleteProduct(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);